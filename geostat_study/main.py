import os
import xml.etree.ElementTree as ET
import numpy as np
import pandas as pd
from time import time
import matplotlib.pyplot as plt
from skgstat import Variogram, OrdinaryKriging

plt.style.use('ggplot')
SCRIPTPATH = os.path.abspath(os.path.dirname(__file__))
RPATH = os.path.join(SCRIPTPATH, '../results')

def xml_to_json(fname):
    # Parse the document
    doc = ET.parse(fname).getroot()
    data = list()
    
    # extract all child from each element
    for element in doc:
        record = {child.tag: child.text for child in element}
        data.append(record)
        
    return data


def get_array_from_json(key, data, force_numeric=False):
    arr = np.asarray([d[key] for d in data])
    if force_numeric:
        arr = np.asarray([np.float(_.replace(',', '.') if _ is not None else np.nan) for _ in arr])
    return arr


def merge_dict(idx1, idx2, values):
    merged = dict()
    
    for i, tup in enumerate(zip(idx1, idx2)):
        i1, i2 = tup
        # does idx1 exist
        if merged.get(i1) is None:
            merged[i1] = dict()
        # does idx2 exist
        if merged.get(i1).get(i2) is None:
            merged[i1][i2] = values[i]
        else:
            # take mean
            merged[i1][i2] = np.mean([merged[i1][i2], values[i]])
    return merged
    
    
def get_input_data(varname):
    # load data
    meta = xml_to_json(os.path.join(SCRIPTPATH, 'data/1977_164_1.xml'))
    data = xml_to_json(os.path.join(SCRIPTPATH, 'data/1977_164_2.xml'))
    
    # locations
    names = get_array_from_json('NAME', meta)
    x = get_array_from_json('X_ETRS89', meta, force_numeric=True)
    y = get_array_from_json('Y_ETRS89', meta, force_numeric=True)
    
    # data entries
    dnames = get_array_from_json('NAME', data)
    depths = get_array_from_json('BOT_DEPTH', data)
    densities = get_array_from_json(varname, data, force_numeric=True)
    
    # merge frame
    merged_data = merge_dict(dnames, depths, densities)
    
    # create an odering index from mergred frame
    merge_names = [_ for _ in merged_data.keys()]
    idx = np.asarray([np.where(names==_)[0] for _ in merge_names]).flatten()
    
    # reorder x and y
    x = x[idx]
    y = y[idx]
    names = names[idx]
    
    # get values
    values = np.asarray([list(merged_data[_].values())[0] for _ in merged_data.keys()])
    
    # get rid of nan
    x = x[np.where(~np.isnan(values))]
    y = y[np.where(~np.isnan(values))]
    values = values[np.where(~np.isnan(values))]
    
    return x, y, values


def plot_overview(x, y, values):
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    
    ax.scatter(x, y, values*20 + 20, alpha=0.5)
    ax.set_xlabel('X location [m]')
    ax.set_ylabel('Y location [m]')
    ax.set_title('Data overview')
    
    return fig
    

def plot_result(x, y, res, xx, yy, cmap='summer'):
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    m = ax.pcolor(xx, yy, res, cmap=cmap)
    plt.colorbar(m, ax=ax)
    
    # get the limits for later
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    ax.plot(x, y, '.k', ms=5)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.grid(which='both')
    ax.set_xlabel('X location [m]')
    ax.set_ylabel('Y location [m]')
    ax.set_title('Interpolation result')
    
    return fig

def load_data():
    df = pd.read_csv(os.path.join(RPATH, 'input_data.csv'))
    x = df.x
    y = df.y
    values = df['values']
    
    return x, y, values


def run_analysis(*args):
    # load data
    print('Search input file...')
    x, y, values = load_data()
    print('Overview plot.')
    fig = plot_overview(x, y, values)
    fig.savefig(os.path.join(RPATH, 'overview.pdf'))
    
    # variogram
    print('fitting variogram')
    V = Variogram(list(zip(x, y)), values, normalize=False, 
              maxlag=20000, estimator='cressie', n_lags=15, 
              use_nugget=True, model='stable'
             )
    print(V)
    fig = V.plot()
    fig.savefig(os.path.join(RPATH, 'variogram.pdf'))
    
    print('start Kriging...')
    ok = OrdinaryKriging(V, min_points=5, max_points=15)
    xx, yy = np.mgrid[400000:550000:100j, 5800000:5850000:100j]
    t0 = time()
    res = ok.transform(xx.flatten(), yy.flatten()).reshape(xx.shape)
    t1 = time()
    print('Took: %.3f sec' % (t1 - t0))
    print('Writing result files')
    np.savetxt(os.path.join(RPATH, 'result_field.dat'), res)
    
    fig = plot_result(x, y, res, xx, yy)
    fig.savefig(os.path.join(RPATH, 'result_map.pdf'))
    
    fig = plot_result(x, y, res, xx, yy, cmap='viridis')
    fig.savefig(os.path.join(RPATH, 'result_map_alternative.pdf'))


if __name__=='__main__':
    import sys
    args = sys.argv
    n = len(args)
    if n < 2:
        print('Please specify the action, path and optional arguments')
        print('example: main.py preprocessing [varname]')
        exit(1)
    # get the action
    action = args[1]
    
    if action.lower() == 'pre' or action.lower() == 'preprocessing':
        print('Parsing input data...')
        varname = args[2] if n >= 3 else 'BULK_DEN'
        x, y, values = get_input_data(varname)
        
        print('Parsed data:')
        print('X: %d  Y: %d  values: %d' %(len(x), len(y), len(values)))
        print('Writing input file...')
        
        resultpath = os.path.join(SCRIPTPATH, '../results/input_data.csv')
        pd.DataFrame({'x':x, 'y':y, 'values': values}).to_csv(resultpath, index=False)
        print('Done.')
    
    elif action.lower() == 'main' or action.lower() == 'run':
        run_analysis(*args[2:])
    
    else:
        print("Only actions are 'preprocessing' and 'run'")
        exit(1)
    