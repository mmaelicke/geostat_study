# Geostatistics Study

## Disclaimer

This repository is demonstrating an easy to implement geostatistical analysis of some soil property data. It is used to demonstrate a scientific workflow common in geosciences. This work is part of my CI-CR best practice paper (in work). While the presented study itself does not reveal anything groundbreaking, it is conducted in a *real world* usecase manner and can easily be substituted by the respective new method, model, application or whatever is going to be published.

The main focus of the CI-CR paper is to foster reproducibility in geoscience. The presented usecase is making use of Python, but the method can easily be applied to other languages. In Python, the interactive feature is called a Jupyter notebook, where text, images, videos and Python code can be used and compiled together. That is the `analysis.ipynb` file. The `main.py` file in the same directory is, on the other hand, is the *scripting* part of this work. It can be run by a Python interpreter to calculate and print results into a `results` folder. We utilize these two concepts to build reproducible and easy-to-copy companion codes for scientific research. This document can be online supplementary material, focusing on implementation and derivation of methods. The script is basically doing the actual analysis and will be run in an external controlled environment on giltab. The respective results can be downloaded, archived and even published with a DOI to make them citeable. 
In gitlab, all necessary information can be copied into a private project and reused with ease.

## Introduction

We present a geostatistical analysis of soil properties reported by Schindler & Müller (2010) from Northern Germany. The authors supply soil properties from 77 locations in the XML format. Among others bulk density and saturated hydraulic conductivity is reported. 
We will estimate spatial covariance in the topmost layer using a variogram. The variogram model best fitting the geostatistical properties of the observed data will be used to create an areal map of soil properties using Ordinary Kriging. The variogram and kriging routines are taken from Mälicke & Schneider (2019).

## References

Schindler, U., & Müller, L. (2010). Data of hydraulic properties of North East and North Central German soils. Earth System Science Data, 2(2), 189–194. https://doi.org/10.5194/essd-2-189-2010

Mälicke, M., & Schneider, H. D. (2019). mmaelicke/scikit-gstat: Version 0.2.5. https://doi.org/10.5281/zenodo.135584

## How to use?

The main advantage of the presented workflow is the ability to [fork](<https://gitlab.com/mmaelicke/geostat_study/forks/new) (copy) the whole project into your own Gitlab account. As soon as it is copied, the CI will run the code and reproduce the outputs. You are free to change any part and rerun the Pipelines. The companion artifacts of each pipeline run include operation system output and log files, which makes a comprehensive understanding of every step taken in the analysis very easy. It is also possible to reproduce the exact OS used in a [Docker](https://docker.com) container. 

The `analysis.ipynb` can either be run directly [here on gitlab](<https://gitlab.com/mmaelicke/geostat_study/blob/master/geostat_study/analysis.ipynb) or after cloning or downloading the repository in a local [Jupyter](<https://jupyter.org/) instance.

The exact commands taken in the pipeline can be found in the `run` subfolder. There are three Python files of which only `pre.py` and `main.py` are of importance. These include the subprocess call to the respective preprocessing and main analysis scripts, which are the same python file in this case (`geostat_study/main.py`). This file offers a very primitive CLI to perform all necessary calculations.

## How to cite?

Well, there is nothing really special about the scientific part of the geostatistical analysis, so no need to cite. Anything concerning the CI-CR idea can be cited by the best practice paper (in work). More info as soon as it is published.

More info on continuous-research.hydrocode.de/help 

